#!/bin/sh
cd "$(dirname "${0}")" || exit 1
#
# https://github.com/yihui/hugo-xmin/blob/master/exampleSite/layouts/partials/foot_custom.html
#

download() {
  curl --location --create-dirs --remote-time --output	"static/js/${1}" --url "${2}"
}

download "math-code.js" 	"https://yihui.name/js/math-code.js"
download "center-img.js"	"https://yihui.name/js/center-img.js"
download "MathJax.js" 		"https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML"
