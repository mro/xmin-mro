<?xml version="1.0" encoding="UTF-8"?>
<!--
  xmin-mro Hugo theme
  Copyright (C) 2019-2019  Marcus Rohrmoser, http://code.mro.name/mro/xmin-mro

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  https://www.w3.org/TR/xslt-10/
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:a="http://www.w3.org/2005/Atom"
  xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:sg="http://purl.mro.name/ShaarliGo/"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="a opensearch media georss sg"
  xmlns:math="http://exslt.org/math"
  extension-element-prefixes="math"
  version="1.0">

  <!-- replace linefeeds with <br> tags -->
  <xsl:template name="linefeed2br">
    <xsl:param name="string" select="''"/>
    <xsl:param name="pattern" select="'&#10;'"/>
    <xsl:choose>
      <xsl:when test="contains($string, $pattern)">
        <xsl:value-of select="substring-before($string, $pattern)"/><br class="br"/><xsl:comment> Why do we see 2 br on Safari and output/@method=html here? http://purl.mro.name/safari-xslt-br-bug </xsl:comment>
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
          <xsl:with-param name="pattern" select="$pattern"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="calculate-day-of-the-week">
    <!-- https://www.oreilly.com/library/view/xslt-cookbook/0596003722/ch03s02.html -->
    <xsl:param name="date-time"/>
    <xsl:param name="date" select="substring-before($date-time,'T')"/>
    <xsl:param name="year" select="substring-before($date,'-')"/>
    <xsl:param name="month" select="substring-before(substring-after($date,'-'),'-')"/>
    <xsl:param name="day" select="substring-after(substring-after($date,'-'),'-')"/>
    <xsl:variable name="a" select="floor((14 - $month) div 12)"/>
    <xsl:variable name="y" select="$year - $a"/>
    <xsl:variable name="m" select="$month + 12 * $a - 2"/>
    <xsl:value-of select="($day + $y + floor($y div 4) - floor($y div 100) + floor($y div 400) + floor((31 * $m) div 12)) mod 7"/>
  </xsl:template>

  <xsl:template name="human_time">
    <xsl:param name="time">-</xsl:param>
    <xsl:variable name="wday">
      <xsl:call-template name="calculate-day-of-the-week">
        <xsl:with-param name="date-time" select="$time"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="0 = $wday">So</xsl:when>
      <xsl:when test="1 = $wday">Mo</xsl:when>
      <xsl:when test="2 = $wday">Di</xsl:when>
      <xsl:when test="3 = $wday">Mi</xsl:when>
      <xsl:when test="4 = $wday">Do</xsl:when>
      <xsl:when test="5 = $wday">Fr</xsl:when>
      <xsl:when test="6 = $wday">Sa</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
    <xsl:text>, </xsl:text>
    <xsl:value-of select="substring($time, 9, 2)"/><xsl:text>. </xsl:text>
    <xsl:variable name="month" select="substring($time, 6, 2)"/>
    <xsl:choose>
      <xsl:when test="'01' = $month">Jan</xsl:when>
      <xsl:when test="'02' = $month">Feb</xsl:when>
      <xsl:when test="'03' = $month">Mär</xsl:when>
      <xsl:when test="'04' = $month">Apr</xsl:when>
      <xsl:when test="'05' = $month">Mai</xsl:when>
      <xsl:when test="'06' = $month">Jun</xsl:when>
      <xsl:when test="'07' = $month">Jul</xsl:when>
      <xsl:when test="'08' = $month">Aug</xsl:when>
      <xsl:when test="'09' = $month">Sep</xsl:when>
      <xsl:when test="'10' = $month">Okt</xsl:when>
      <xsl:when test="'11' = $month">Nov</xsl:when>
      <xsl:when test="'12' = $month">Dez</xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose><xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 4)"/><xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 12, 5)"/><!-- xsl:text> Uhr</xsl:text -->
  </xsl:template>

  <xsl:template name="degrees">
    <xsl:param name="num" select="0"/>
    <xsl:choose>
      <xsl:when test="$num &lt; 0">-<xsl:call-template name="degrees"><xsl:with-param name="num" select="-$num"/></xsl:call-template></xsl:when>
      <xsl:when test="$num &gt;= 0">
        <xsl:variable name="deg" select="floor($num)"/>
        <xsl:variable name="min" select="floor(($num * 60) mod 60)"/>
        <xsl:variable name="sec" select="format-number((($num * 36000) mod 600) div 10, '0.0')"/>
        <xsl:value-of select="$deg"/>° <!--
        --><xsl:value-of select="$min"/>' <!--
        --><xsl:value-of select="$sec"/>"
      </xsl:when>
      <xsl:otherwise>?</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:variable name="base_url">../../../../</xsl:variable>

  <xsl:template match="/">
    <xsl:apply-templates select="a:feed"/>
  </xsl:template>

  <xsl:template match="a:feed">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>

        <link rel="stylesheet" href="{$base_url}assets/style.css"/>
        <style type="text/css">
        </style>
        <title><xsl:value-of select="a:title"/></title>
      </head>

      <body>
        <p style="text-align:center">* * *</p>
        <h2>Comments <a href="comments.xml" target="_top"><img src="{$base_url}assets/feed-icon.svg#height24" alt="Feed Icon" title="Comment Feed"/></a></h2>
        <ol id="entries" start="0">
          <li><a href="mailto:Thua4aehah4ae2tiem@mro.name?subject=Comment {a:id}..&amp;body={a:title}%0a[x] please publish this comment under the original post including my email sender name but not my email address.%0a%0a…">Comment via Email 📧</a></li>
          <xsl:apply-templates select="a:entry">
            <xsl:sort select="a:updated" data-type="text" order="ascending"/>
          </xsl:apply-templates>
        </ol>
        <script type="text/javascript">
//<![CDATA[
// console.log('make http and geo URIs (RFC 5870) clickable + microformat');
const elmsRendered = document.getElementById('entries').getElementsByClassName('rendered');
requestAnimationFrame(function() {
  for (var i = 0; i < elmsRendered.length; i++) {
    const elm = elmsRendered[i];
    elm.innerHTML = elm.innerHTML.replace(/(https?:\/\/([^ \t\r\n"'<]+[^ \t\r\n"'.,;()<]))/gi, '<a rel="noreferrer" target="_parent" class="http" href="$1">$2</a>');
    // https://alanstorm.com/url_regex_explained/ \b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))
    // elm.innerHTML = elm.innerHTML.replace(/\b(([\w-]+:\/\/?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/)))/gi, '<a rel="noreferrer" class="http" href="$1">$1</a>');
    elm.innerHTML = elm.innerHTML.replace(/geo:(-?\d+.\d+),(-?\d+.\d+)(\?z=(\d+))?/gi, '<a rel="noreferrer" target="_parent" class="geo" href="https://opentopomap.org/#marker=12/$1/$2" title="zoom=$4">geo:<span class="latitude">$1</span>,<span class="longitude">$2</span>$3</a>');
  }
});
//]]>
        </script>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="a:entry">
    <xsl:variable name="redirector"></xsl:variable>
    <xsl:variable name="link" select="a:link[not(@rel)]/@href"/>
    <xsl:variable name="self" select="a:link[@rel='self']/@href"/>
    <xsl:variable name="id_slash" select="substring-after($self, '/p/')"/>
    <xsl:variable name="id" select="substring-before($id_slash, '/')"/>
    <li id="{$id}">
      <p>
        <xsl:variable name="entry_updated" select="a:updated"/>
        <xsl:variable name="entry_updated_human"><xsl:call-template name="human_time"><xsl:with-param name="time" select="$entry_updated"/></xsl:call-template></xsl:variable>
        <a href="{a:id}"><xsl:value-of select="$entry_updated_human"/></a>,
        <a target="_parent">
          <xsl:variable name="author_uri" select="a:author/a:uri"/>
          <xsl:if test="$author_uri">
            <xsl:attribute name="href">
              <xsl:value-of select="$author_uri"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="a:author/a:name"/>
        </a>:
      </p>
      <p class="rendered type-text">
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="a:content"/>
        </xsl:call-template>
      </p>
   </li>
  </xsl:template>
</xsl:stylesheet>
