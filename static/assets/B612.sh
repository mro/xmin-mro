#!/bin/sh
cd "$(dirname "${0}")" || exit 1

for fo in Regular Bold Italic BoldItalic
do
  for fmt in woff2 woff
  do
    f="B612-${fo}.${fmt}"
    curl --location --create-dirs --output "./${f}" --remote-time --time-cond "assets/${f}" "http://b612-font.com/fonts/${f}"
  done
done
